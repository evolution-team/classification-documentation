# Hodnocení předmětu

V menu po levé straně stránky se vyskytuje seznam předmětů, které vyučujete.
Máte-li na to právo, můžete nastavit hodnocení předmětů (vizte definice hodnocení).

Po vyplnění a uložení hodnocení se zobrazí potvrzovací dialog, ve kterém máte možnost studenty, jimž se změnilo hodnocení, upozornit na nová hodnocení e-mailem.
Tento e-mail se pošle, pokud zaškrtnete, že chcete upozornit studenty.
Zároveň se pošle upozornění v aplikaci.
Student po ohodnocení také uvidí datum poslední změny upraveného hodnocení.

S vytvořenými definicemi je možné hodnotit studenty několika způsoby, které najdete rozbalené pod názvem předmětu po kliknutí na předmět v menu.
Dále následuje jejich popis.

## Hodnocení předmětu

**Skupina studentů.**
V prvé řadě se musí vybrat skupina studentů, kterou chcete hodnotit.
Skupinami jsou například jednotlivé paralelky předmětu, jednorázové akce nebo zkouškové a zápočtové termíny.
Navíc jsou na výběr i umělé skupiny jako například "Všechny moje cvičební paralelky", které seskupují studenty paralelky jednoho druhu.
V neposlední řadě je možnost zobrazit si umělé studenty, kteří jsou určeni k testování.

**Zobrazování sloupců.**
Zobrazování sloupců lze měnit, příslušná menu najdete po kliknutí na "Zobrazené údaje o studentech" nebo "Zobrazené sloupce".
Při horizontálním scrollování jsou zobrazené údaje o studentech vždy viditelné kvůli přehlednosti, aby se nemuselo při hodnocení sloupců na pravém konci tabulky neustále přejíždět a zjišťovat, kdo je zrovna hodnocen.
Podle sloupců je možné tabulku řadit, v jednu chvíli lze řadit podle nejvýše jednoho sloupce.
Toho se docílí kliknutím na příslušný název sloupce v záhlaví tabulky.
Dalším kliknutím změníte pořadí výpisu (z vzestupného na sestupné) a třetím kliknutím řazení vypnete.
Tento cyklus lze opakovat.
Tlačítkem "Přepnout čísla řádků" lze zobrazit či vypnout čísla řádků tabulky.

**Vyhledávání studentů.**
V tabulce lze vyhledávat podle jména, příjmení a usernamu studentů, k čemuž slouží textový vstup nad tabulkou.
Při vyhledávání se ignoruje diakritika a na pořadí vyhledávaných jmen nezáleží.
Jména se oddělují mezerou.
Implicitní logická spojka je logický and, tzn. výsledkem dotazu budou studenti, kteří mají všechna dotazovaná jména jako podřetězec ve jméně a usernamu.
Vyhledávání se provede samo, není potřeba klikat na tlačítko "Najít".

Další možností vyhledávání je vyhledávání podle výrazu.
Dostanete se na něj kliknutím na tlačítko "Hledat výrazem".
Zobrazí se editor, kam můžete dotaz vložit v podobě jednoduchého programovacího jazyka, který je totožný s jazykem pro vytváření vzorců.
Vyhledávaný výraz se vyhodnotí a zůstanou zobrazeni pouze ti studenti, pro něž je splněna podmínka daná výrazem.  
*Příklad 1*: pokud je definovaný sloupec s identifikátorem `zap` a datovým typem boolean a hledaným výrazem je `!zap`, pak se zobrazí studenti, jejichž hodnota proměnné `zap` je false nebo null.  
*Příklad 2*: pokud je definované sloupce s identifikátory `zap` a `celk_hod` a hledaným výrazem je `zap && celk_hod >=10`, pak se zobrazí studenti, jejichž hodnota proměnné `zap` je true a zároveň hodnota proměnné `celk_hod` je alespoň 10.  
Vyhledávání se provede samo, není potřeba klikat na tlačítko "Najít".

**Klávesové zkratky.**
V hodnotící tabulce se můžete mezi políčky se vstupy pohybovat doleva a doprava klávesami `tab`, resp. `shift + tab`.
Pohyb nahoru a dolů se provádí klávesami `up` a `down`.
Zkratkou `shift + enter` se provádí uložení.

**Stránkování tabulky.**
Tabulka se kvůli výkonu stránkuje.
Mezi stránkami se přepíná kliknutím na čísla stránek nebo na šipky pod tabulkou.
Můžete si nastavit počet zobrazených položek po kliknutí na tlačítko udávající aktuální počet zobrazených položek na stránku.
Také je k dispozici tlačítko na zobrazení celé tabulky.
Nastavení velikosti stránky se ukládá do cookies, zobrazení všeho však ne.

**Obsah hodnotící tabulky.**
Nejdůležitější komponentou této stránky je hodnotící tabulka, kde řádky jsou záznamy jednotlivých studentů a sloupci jsou definice předmětu nebo osobní údaje studentů.
Pokud se hodnocení některé definice zadává ručně, pak v tabulce je v odpovídajícím sloupci vstup, který závisí na datovém typu.
O datových typech se lze dočíst v dokumentaci definice hodnocení.
Speciálně poznamenejme, že booleany jsou třístavové - mohou nabývat hodnot true, false a null.

### Import a export

Menu importu a exportu se schovává pod tlačítkem "Import / export".

**Formát exportované csv tabulky.**
Exportovaná tabulka je, co se týče struktury, podobná hodnotící tabulce.
Na prvním řádku se očekávají názvy proměnných definic a proměnné reprezentující osobní údaje studentů.
Seznam proměnných pro osobní údaje je na konci tohoto dokumentu.
Na dalších řádcích následují záznamy jednotlivých studentů, přičemž pořadí údajů musí odpovídat pořadí záhlaví.

**Importování z csv.**
Oddělovač sloupců se při importu rozezná automaticky.
V importovaném souboru musí být přítomen sloupec s usernamem.
K porozumění zbytku této části si představte, že se import provádí přepisováním obsahu souboru do hodnotící tabulky řádek po řádku.
Ze souboru se importují pouze ty řádky, které jsou zrovna viditelné v tabulce na stránce hodnocení předmětu (záleží tedy na současně zobrazené skupině studentů, velikosti stránky, atd.).
Staré údaje v tabulce se přepisují novými hodnotami.
Pokud jsou zobrazení studenti, kteří nemají záznam v importovaném souboru, pak se jejich hodnocení nijak nemění.
Položky, které jsou pouze ke čtení, také zůstanou nezměněné.
**Pozor**, po provedení importu je potřeba tabulku uložit.
Po provedení importu v tabulce zůstanou viditelní pouze ti studenti, kterým se nějaké hodnocení změnilo.


**Exportování do csv.**
Tabulku si můžete exportovat do formátu csv, její struktura je popsána výše.
Při exportování si můžete vybrat název výsledného souboru a použitý oddělovač.

## Podrobné hodnocení

Podrobné hodnocení slouží k ohodnocení jedné definice.
Oproti hodnocení předmětu je v podrobném hodnocení možnost připsat poznámku k hodnocení, kterou si student může zobrazit.
U zvolené definice můžete vidět její informace jako například identifikátor, vztah k předmětu, datový typ, atd.
Zbývající funkcionality jsou totožné jako v hodnocení předmětu, pokud jsou přítomny.

## Hodnocení studenta

Hodnocení studenta slouží k ohodnocení jednoho studenta.
V tabulce na této stránce jsou k dispozici všechny definice předmětu.
Oproti hodnocení předmětu je v hodnocení studenta možnost připsat poznámku k hodnocení, kterou si student může zobrazit.
Také je zobrazeno datum poslední změny u jednotlivých definic.
Zbývající funkcionality jsou totožné jako v hodnocení předmětu, pokud jsou přítomny.

## Proměnné pro osobní údaje v exportu do csv

Je potřeba dodržovat velká a malá písmena v proměnných!

<table>
  <thead>
    <tr>
      <th>Osobní údaj</th>
      <th>Proměnná v exportu</th>
    </tr>
  </thead>
  <tr>
    <td>Celé jméno</td>
    <td>fullName</td>
  </tr>
  <tr>
    <td>ČVUT username</td>
    <td>username</td>
  </tr>
  <tr>
    <td>Osobní číslo</td>
    <td>personalNumber</td>
  </tr>
  <tr>
    <td>Ročník</td>
    <td>grade</td>
  </tr>
  <tr>
    <td>Číslo přednáškové paralelky</td>
    <td>LECTURE_parallelNumber</td>
  </tr>
  <tr>
    <td>Číslo cvičební paralelky</td>
    <td>TUTORIAL_parallelNumber</td>
  </tr>
  <tr>
    <td>Číslo laboratorní paralelky</td>
    <td>LABORATORY_parallelNumber</td>
  </tr>
  <tr>
    <td>Číslo seminární paralelky</td>
    <td>SEMINAR_parallelNumber</td>
  </tr>
  <tr>
    <td>Číslo konzultační paralelky</td>
    <td>CONSULTATION_parallelNumber</td>
  </tr>
  <tr>
    <td>ID přednáškové paralelky</td>
    <td>LECTURE_parallelCode</td>
  </tr>
  <tr>
    <td>ID cvičební paralelky</td>
    <td>TUTORIAL_parallelCode</td>
  </tr>
  <tr>
    <td>ID laboratorní paralelky</td>
    <td>LABORATORY_parallelCode</td>
  </tr>
  <tr>
    <td>ID seminární paralelky</td>
    <td>SEMINAR_parallelCode</td>
  </tr>
  <tr>
    <td>ID konzultační paralelky</td>
    <td>CONSULTATION_parallelCode</td>
  </tr>
</table>
