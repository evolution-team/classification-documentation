# API

K datovým zdrojům Grade je možné přistupovat přes REST api. Api je **dostupné všem uživatelům** (studentům také).

Api je také dostupné **externím aplikacím** schváleným z Grades pro předmět garantem (nebo editorem).
Vhodné pro automatické vyhodnocovače jako Progtest nebo Marast.

Je vyžadován validní **bearer token** (více zde <https://rozvoj.fit.cvut.cz/Main/oauth2>).
Pro uživatelský přístup použijte scopy `user-read` a `user-write`, pro přístup aplikací scope `course-restricted`.
Podrobný návod bude brzy napsán. S žádostí o pomoc neváhejte kontaktovat vývojový tým (kontakt níže).

**Api je téměř finální a podrobná dokumentace je postupně psána**

Dokumentace s rozhraním pro vyzkoušení dotazů je zde: <https://grades.fit.cvut.cz/api/v1/swagger-ui.html>

**Neváhejte požádat vývojový tým o pomoc**. Napiště na plachste(at)fit.cvut.cz
