# Dokumentace definice hodnocení předmětu
## Vytvoření definice

* **Název česky** - Český název hodnocení
* **Název anglicky** - Anglický název hodnocení
* **Identifikátor** - Identifikátor slouží jako proměnná pro dané hodnocení. Používá se v hodnocení, které se vypočítá podle zadaného vzorce. Příklad zadávání vzorců najdete níže.
* **Vztah k předmětu** - Vztah k předmětu slouží jako metadata k danému hodnocení. Není příliš podstatné k funkci hodnocení, ale pomůže tvoření statistik hodnocení předmětů.
* **Datový typ** - Vzhledem k tomu, že hodnocení podporuje zadávání vzorců, je potřeba ke každé proměnné definovat její datový typ.
* **Způsob zadání** - Způsob zadání definuje, jestli hodnotu budou zadávat vyučující ručně nebo se vypočítá z ostatních hodnocení vzorcem nebo agregací (více v sekcích níže).
* **Viditelnost pro studenty** - V některých případech si vyučující potřebuje ke studentům udělat
        'soukromou' poznámku nebo pomocnou informaci.

Pro datový typ *číslo*:
* **Minimální požadovaná hodnota** - Minimální požadovaná hodnota ukazuje studentům, jaké je pro
        číselné
        hodnocení požadované minimum. Není to povinná hodnota. Pokud je student ohodnocen hodnocením menším než je
        minimum, je na to učitel i student graficky upozorněn. Minimální požadova hodnota nijak nezasahuje do udělování
        zápočtů nebo známky, tato podmínka musí být v příslušném vzorci definovaná znovu.
* **Maximální hodnota** - Maximální požadovaná hodnota ukazuje studentům, kolik lze získat z
        hodnocení
        maximum bodů. Není to povinná hodnota. Pokud je student ohodnocen hodnocením větším než je maximum, je na to
        učitel graficky upozorněn (Zadávání většího hodnocení je povoleno).
* **Minimální hodnota** - Za minimální hodnotu se implicitně považuje nula. Vyplnění této položky je tedy vhodné, pokud
        může mít hodnocení např. i zápornou hodnotu.

Pro datový typ *text*:
* **Omezení hodnot** - Omezí textové hodnoty, které je možné ručně zadat.
        Pokud není prázdné, v tabulce hodnocení bude místo textového vstupu výběr z těchto hodnot.
        Vhodné např. pro ručně zadávanou známku (omezení na hodnoty A, B, ..., F).

<img alt="Nový sloupec hodnocení" src="https://gitlab.fit.cvut.cz/evolution-team/classification-documentation/raw/master/img/new_column.png"/>

## Uspořádání definic hodnocení

Je množné měnit pořadí definic a zanořovat je pod sebe.
**Uspořádání** má pouze **vizuální vliv** a nijak **neovlivňuje výpočet vzorců**.

**Zanořování** je vhodné použít pro zpřehlednění, například pro **seskupení podobných druhů hodnocení**.
Například, pokud máme několik testů, je vhodné je zanořit pod sloupec s jejich celkovým součtem bodů.
Pro přímé vytvoření nového sloupce pod jiným je u sloupce tlačítko pro přidání podsloupce.

<img alt="Přidání podsloupce" src="https://gitlab.fit.cvut.cz/evolution-team/classification-documentation/raw/master/img/add_subcolumn.png"/>

Zanořené definice se zobrazí v tabulce hodnocení v **postfixovém pořadí**, tedy nejprve podsloupce následované jejich nadsloupcem.

Změna uspořádání již vytvořených podsloupců je možná **přetažením myší** po zapnutí funkce **drag'n'drop**.
Přetažením je možné také zanořit sloupec pod jiný.

<img alt="drag'n'drop" src="https://gitlab.fit.cvut.cz/evolution-team/classification-documentation/raw/master/img/drag_n_drop.png"/>


## Import definic hodnocení

Importovat definice hodnocení lze z existujícího **předmětu** (v semestru), či **CSV** souboru. 

Definovat zadání je možno také importováním zadání z jiného předmětu/semestru. Této funkce se dá vyžít k importu
definic hodnocení z minulého semestru, popřípadě z anglické/české/kombinované verze předmětu. Při importování se
volí možnost přepsání stávajících hodnocení. Nepředpokládá se, že by se import prováděl v době, kdy už nejaká
hodnocení existují, ale je nutné toto ošetřit. Možnost smazání stávajících zadání smaže všechny definice i
studentská hodnocení v současném semestru a nahradí je importem, možnost nesmazání zachová stávající hodnocení a
pouze přidá ty, které nemají shodný identifikátor s již existujícími.

Importovat lze pouze z předmětů a semestrů, které má učitel právo vidět - byl v zadaném semestru a předmětu
vyučující.

<img alt="Import definice" src="https://gitlab.fit.cvut.cz/evolution-team/classification-documentation/raw/master/img/import_definition.png"/>


## Vzorce

Podrobnosti k vytváření vzorců jsou více rozvedeny v sekci *Tvorba vzorců*.
K vytvoření definic předmětu můžete využít *Průvodce tvorbou klasifikace*. Po dokončení lze definice dále upravovat dle libosti.

<img alt="Náhled průvodce tvorbou klasifikace" src="https://gitlab.fit.cvut.cz/evolution-team/classification-documentation/raw/master/img/guide.png"/>


Na následujícím příkladu je demonstrována definice ukázkového předmětu.
V tomto předmětu se píše jeden semestrální test ve kterém je potřeba získat alespoň 10 z 20 možných bodů.
Na zápočet je potřeba úspěšně absolvovat semestrální test.
Při splnění zápočtu se píše zkouškový test, který má dva termíny, a student má dva pokusy. Minimální hodnota z posledního pokusu absolvované zkoušky pro získání známky je 40 z 80 bodů.
Při splnění podmínek získání zápočtu a složení zkoušky je student hodnocen podle standardní stupnice ČVUT: (∞,50) -
F, <50-60) - E, <60-70) - D, <70-80) - C, <80-90) - B, <90,∞) - A.

<img alt="Ukázkový předmět" src="https://gitlab.fit.cvut.cz/evolution-team/classification-documentation/raw/master/img/formula.png"/>


V příkladu můžeme vidět použití regulárních výrazů (`exam\d+` = exam1,exam2,exam3), funkcí, operátorů.
Každý vzorec je v reálném čase kontrolován a uživatel je informován o jeho validitě. Zároveň může využít nápovědy,
která se snaží nasměrovat k místě případné chyby.


## Agregace

Agregace dovolují výpočet funkce napříč skupinou studentů. Lze tedy například spočítat součet získaných bodů všech studentů paralelky.
To lze využít například pro přerozdělení pevně daného počtu bodů pro paralelku mezi studenty (takzvaný měšec), čehož lze dosáhnout tím,
že studentům jsou rozdávány virtuální body, které se poté v závislosti na jejich celkovém rozdaném množství přepočítají na skutečné body.

Je nutné zvolit sloupec který chceme agregovat a jednu funkci. Složitější vzorce v agregačních sloupcích nejsou možné,
lze s nimi ale poté pracovat ve vzorcích jako s jakýmikoli jinými položkami hodnocení.

Pro agregaci je nutné vyplnit:

* **Agregovaný sloupec** - kterýkoli sloupec, který již není agregací, na který se aplikuje agregační funkce.
* **Agregační funkce** - jsou filtrovány na základě vybraného datového typu a datového typu agregovaného sloupce
* **Rozsah agregace** - definuje skupiny studentů, přes které se agreguje, což je buď celý předmět nebo jednotlivé druhy paralelek
        (přednáška, cvičení, laboratoř). Jsou zobrazeny pouze ty, které má předmět napsány v KOSu.

### Omezení agregací
Agregace se přepočítávají pouze u předmětů v současném nebo následujícím semestru.
Je to opatření k zachování hodnocení studentů po skončení semestru, kdy informace o paralelkách nemusí být úplné.
