# Uživatelské role a jejich oprávnění

## Uživatelské role vztažené k jednotlivým předmětům

Následující role se vždy vztahují ke konkrétnímu předmětu. Např. být učitel předmětu BI-AAG neznamená být učitelem předmětu BI-PA1, atd. 

Seznam rolí:
* učitel,
* editor,
* garant.

Seznam akcí:
* hodnotit studenty,
* definovat hodnocení předmětu,
* přidávat další editory.

Učitelé mohou studenty pouze hodnotit. Nehodnotí však pouze studenty, které jsou v jeho paralelce, ale libovolného studenta předmětu.

Editor má oproti učiteli navíc možnost definovat hodnocení předmětu. Pokud je uživatel editorem předmětu, pak je implicitně i učitelem předmětu.

Garant předmětu má oproti editorovi možnost přidávat k předmětu další editory.

Shrnutí je v tabulce níže.

|        | Hodnotit studenty | Definovat hodnocení předmětu | Přidávat editory |
|:------:|:-----------------:|:----------------------------:|:----------------:|
| Učitel |        [x]        |                              |                  |
| Editor |        [x]        |              [x]             |                  |
| Garant |        [x]        |              [x]             |        [x]       |

Role učitel a garant se přiděluje automaticky podle informací v KOSu. Roli editor musí garant manuálně udělit učitelům předmětu. Role editor se vztahuje na všechny semestry, stačí ji udělit pouze jednou.

## Uživatelské role nezávislé na předmětech

Pokud má uživatel jednu z rolí uvedených v této sekci, pak ji má nezávisle na předmětu.

Seznam rolí:
* vedoucí katedry,
* katederní rozvrhář.

Vedoucí katedry se chová jako garant všech předmětů, které spadají pod jeho katedru.

Tyto role jsou přidělovány podle údajů z KOSu, katederní rozvrhář je podobně jako editor interní role a musí být manuálně určen vedoucím katedry a také není závislá na semestru.

Editorů i katederních rozvrhářů může být více.
