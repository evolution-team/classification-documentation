# Výrazy

Výrazy pro sloupce s funkcionalitou

* jednoduchá podmínka zápočtu -- `(body + test) >= 20`
* sečíst mnoho sloupců -- ``SUM(`sloupec\d+`)`` *argumenty dány regulárním výrazem, popsáno níže*
* sečíst počet výskytů 'N' v mnoha sloupcích -- ``COUNTOCCURENCES('N',`sloupec\d+`)``
* z pokusů vybrat poslední vyplněný -- ``LAST(`pokus\d+`)`` *pokusy řadí abecedně dle identifikátoru!*
* z bodů udělat známku -- `MARK(body)`
* uživatelské jméno studenta -- `$USERNAME`
* požadované minimum ze sloupce *test1* -- `test1.MIN_REQUIRED`

## Formální specifikace výrazů

* Syntaxe výrazů je z velké části založena na syntaxi jazyka C.
* Jazyk je **silně typovaný**. Při vytváření definice hodnocení se kromě unikátního identifikátoru volí i datový typ.
* Datové typy jsou
    * číslo (vnitřně *double*),
    * řetězec,
    * boolean.
* **Boolean** je **třístavový** (true, false a null).
* **Identifikátory** proměnných a funkcí, a klíčová slova jsou **case insensitive**.
* Nedefinovaná hodnota je *null*. V aritmetických a logických operacích (kromě porovnání na rovnost) se chová jako 0, prázdný řetězec nebo false.
* Na **pořadí výrazů nezáleží**, ve výrazech se ale **nesmí nacházet cyklické závislosti** proměnných.
(např. pokud dva sloupce ve svých vzorcích používají identifikátor toho druhého, pak neexistuje validní pořadí vyhodnocení).
* **Nesprávné výrazy** se automaticky **vyhodnocují na null**.
* K dispozici jsou **interní proměnné**  s informacemi o studentech a předmětu **začínající symbolem $**
a informacemi o definicích sloupců ve tvaru **[identifikátor].[proměnná]**

## Operátory

<table>
    <tr>
        <th>priorita</th>
        <th>Operátor</th>
        <th>Arita</th>
        <th>Syntaxe</th>
        <th>Význam</th>
    </tr>
    <tr>
        <td>max</td>
        <td>!</td>
        <td>unární</td>
        <td>!boolean</td>
        <td>logická negace</td>
    </tr>
    <tr>
        <td></td>
        <td>-</td>
        <td>unární</td>
        <td>-číslo</td>
        <td>unární mínus</td>
    </tr>
    <tr>
        <td></td>
        <td>+</td>
        <td>unární</td>
        <td>+číslo</td>
        <td>unární plus</td>
    </tr>
    <tr>
        <td></td>
        <td>^</td>
        <td>binární</td>
        <td>číslo^číslo<br>řetězec^číslo</td>
        <td>mocnění<br>mocnění (zopakování řetězce daný počet-krát)</td>
    </tr>
    <tr>
        <td></td>
        <td>* / %</td>
        <td>binární</td>
        <td>číslo * číslo</td>
        <td>násobení, dělení, modulo</td>
    </tr>
    <tr>
        <td></td>
        <td>+ -</td>
        <td>binární</td>
        <td>číslo + číslo<br>řetězec + řetězec</td>
        <td>sčítání, odčítání<br>zřetězení</td>
    </tr>
    <tr>
        <td></td>
        <td>> < >= <=</td>
        <td>binární</td>
        <td>číslo > číslo</td>
        <td>porovnání</td>
    </tr>
    <tr>
        <td></td>
        <td>== !=</td>
        <td>binární</td>
        <td>výraz == výraz</td>
        <td>rovnost, nerovnost. Lze porovnávat na null</td>
    </tr>
    <tr>
        <td></td>
        <td>&&</td>
        <td>binární</td>
        <td>boolean && boolean</td>
        <td>logické and</td>
    </tr>
    <tr>
        <td></td>
        <td>||</td>
        <td>binární</td>
        <td>boolean || boolean</td>
        <td>logické or</td>
    </tr>
    <tr>
        <td>min</td>
        <td>?:</td>
        <td>ternární</td>
        <td>boolean ? výraz : výraz</td>
        <td>pokud je pravdivá podmínka, pak první výraz, jinak druhý</td>
    </tr>
</table>

Kulaté závorky upravují prioritu operátorů

## Funkce

<table>
    <tr>
        <th>Funkce</th>
        <th>Návratový typ</th>
        <th>Počet parametrů</th>
        <th>Syntaxe</th>
        <th>Význam</th>
    </tr>
    <tr>
        <td>MIN</td>
        <td>číslo</td>
        <td>variabilní</td>
        <td>MIN(číslo, ...)</td>
        <td>minimum</td>
    </tr>
    <tr>
        <td>MAX</td>
        <td>číslo</td>
        <td>variabilní</td>
        <td>MAX(číslo, ...)</td>
        <td>maximum</td>
    </tr>
    <tr>
        <td>SUM</td>
        <td>číslo</td>
        <td>variabilní</td>
        <td>SUM(číslo, ...)</td>
        <td>suma</td>
    </tr>
    <tr>
        <td>AVG</td>
        <td>číslo</td>
        <td>variabilní</td>
        <td>AVG(číslo, ...)</td>
        <td>aritmetický průměr</td>
    </tr>
    <tr>
        <td>CONCAT</td>
        <td>řetězec</td>
        <td>variabilní</td>
        <td>CONCAT(řetězec, ...)</td>
        <td>zřetězení</td>
    </tr>
    <tr>
        <td>PRODUCT</td>
        <td>číslo</td>
        <td>variabilní</td>
        <td>PRODUCT(číslo, ...)</td>
        <td>produkt</td>
    </tr>
    <tr>
        <td>OR</td>
        <td>boolean</td>
        <td>variabilní</td>
        <td>OR(boolean, ...)</td>
        <td>logické or</td>
    </tr>
    <tr>
        <td>AND</td>
        <td>boolean</td>
        <td>variabilní</td>
        <td>AND(boolean, ...)</td>
        <td>logické and</td>
    </tr>
    <tr>
        <td>ROUND</td>
        <td>číslo</td>
        <td>1<br>2</td>
        <td>ROUND(číslo)<br>ROUND(číslo, číslo)</td>
        <td>standardní zaokrouhlení na celá čísla<br>standardní zaokrouhlení na daný počet desetinných míst</td>
    </tr>
    <tr>
        <td>FLOOR</td>
        <td>číslo</td>
        <td>1</td>
        <td>FLOOR(číslo)</td>
        <td>dolní celá část</td>
    </tr>
    <tr>
        <td>CEIL</td>
        <td>číslo</td>
        <td>1</td>
        <td>CEIL(číslo)</td>
        <td>horní celá část</td>
    </tr>
    <tr>
        <td>MARK</td>
        <td>řetězec</td>
        <td>1</td>
        <td>MARK(číslo)</td>
        <td>známka z počtu bodů podle studijního řádu ČVUT</td>
    </tr>
    <tr>
        <td>LAST</td>
        <td>typ</td>
        <td>variabilní</td>
        <td>LAST(typ, ...)</td>
        <td>poslední definovaná (ne null) hodnota</td>
    </tr>
    <tr>
        <td>VALID</td>
        <td>boolean</td>
        <td>variabilní</td>
        <td>VALID(typ, ...)</td>
        <td>pravda pokud všechny argumety jsou definované (ne null)</td>
    </tr>
    <tr>
        <td>COUNT</td>
        <td>číslo</td>
        <td>variabilní</td>
        <td>COUNT(typ, ...)</td>
        <td>počet argumentů (vhodné pro regulární výrazy)</td>
    </tr>
    <tr>
        <td>COUNTVALID</td>
        <td>číslo</td>
        <td>variabilní</td>
        <td>COUNTVALID(typ, ...)</td>
        <td>počet definovaných (ne null) argumentů</td>
    </tr>
    <tr>
        <td>COUNTOCCURENCES</td>
        <td>číslo</td>
        <td>variabilní</td>
        <td>COUNTOCCURENCES(typ, typ, ...)</td>
        <td>kolik hodnot je stejných jako první argument</td>
    </tr>
    <tr>
        <td>ITH</td>
        <td>typ</td>
        <td>variabilní</td>
        <td>ITH(číslo, ...)</td>
        <td>vrátí i-tý definovaný prvek, jinak null. Indexuje se od jedničky.</td>
    </tr>
    <tr>
        <td>LASTIDX</td>
        <td>číslo</td>
        <td>variabilní</td>
        <td>LASTIDX(typ, ...)</td>
        <td>index posledního definovaného argumentu</td>
    </tr>
    <tr>
        <td>TOSTRING</td>
        <td>číslo<br>boolean</td>
        <td>1</td>
        <td>TOSTRING(číslo)<br>TOSTRING(boolean)</td>
        <td>
            textová reprezentace čísla nebo null, pokud je argument null
            <br>
            řetězec "true", "false" nebo "null"
        </td>
    </tr>
    <tr>
        <td>TONUMBER</td>
        <td>řetězec<br>boolean</td>
        <td>1</td>
        <td>TONUMBER(řetězec)<br>TONUMBER(boolean)</td>
        <td>
            textová reprezentace čísla nebo null, pokud formát čísla není validní. Více o formátech
            <a href="https://docs.oracle.com/javase/7/docs/api/java/lang/Double.html#valueOf(java.lang.String)">
                zde
            </a>
            <br>
            1 pro true, 0 pro false, jinak null
        </td>
    </tr>
    <tr>
        <td>TOBOOLEAN</td>
        <td>číslo<br>řetězec</td>
        <td>1</td>
        <td>TOBOOLEAN(číslo)<br>TOBOOLEAN(řetězec)</td>
        <td>
            null, pokud je argument null, nebo false, pokud je argument roven nule, jinak true
            <br>
            přiřadí příslušnou hodnotu, pokud je argument "true" nebo "false" (case insensitive), jinak null
        </td>
    </tr>
    <tr>
        <td>LENGTH</td>
        <td>číslo</td>
        <td>1</td>
        <td>LENGTH(řetězec)</td>
        <td>
            délka řetězce
        </td>
    </tr>
    <tr>
        <td>SUBSTRING</td>
        <td>řetězec</td>
        <td>3</td>
        <td>SUBSTRING(řetězec, číslo, číslo)</td>
        <td>
            podřetězec začínající na pozici v druhém parametru (včetně) a končící před pozicí v třetím parametru, null při nevalidních mezích
        </td>
    </tr>
    <tr>
        <td>PREFIX</td>
        <td>řetězec</td>
        <td>2</td>
        <td>PREFIX(řetězec, číslo)</td>
        <td>
            prefix končící před pozicí v druhém parametru (prefix dané délky), null při nevalidní mezi. Ekvivalentní se SUBSTRING(řetězec, 0, číslo)
        </td>
    </tr>
    <tr>
        <td>SUFFIX</td>
        <td>řetězec</td>
        <td>2</td>
        <td>SUFFIX(řetězec, číslo)</td>
        <td>
            suffix začínající na pozici v druhém parametru (včetně), null při nevalidní mezi. Ekvivalentní se SUBSTRING(řetězec, číslo, LENGTH(řetězec))
        </td>
    </tr>
    <tr>
        <td>CONTAINS</td>
        <td>boolean</td>
        <td>2</td>
        <td>CONTAINS(řetězec, řetězec)</td>
        <td>
            zda řetězec v prvním parametru obsahuje řetězec v druhém parametru jako podřetězec
        </td>
    </tr>
    <tr>
        <td>STARTSWITH</td>
        <td>boolean</td>
        <td>2</td>
        <td>STARTSWITH(řetězec, řetězec)</td>
        <td>
            zda řetězec v druhém parametru je prefixem řetězce v prvním parametru
        </td>
    </tr>
    <tr>
        <td>ENDSWITH</td>
        <td>boolean</td>
        <td>2</td>
        <td>ENDSWITH(řetězec, řetězec)</td>
        <td>
            zda řetězec v druhém parametru je suffixem řetězce v prvním parametru
        </td>
    </tr>
    <tr>
        <td>MATCHES</td>
        <td>boolean</td>
        <td>2</td>
        <td>MATCHES(řetězec, řetězec)</td>
        <td>
            zda řetězec v prvním parametru odpovídá regulárnímu výrazu v řetězci v druhém parametru. O regulárních výrazech více
            <a href="https://docs.oracle.com/javase/7/docs/api/java/util/regex/Pattern.html">
                zde
            </a>
        </td>
    </tr>
    <tr>
        <td>TOLOWERCASE</td>
        <td>řetězec</td>
        <td>1</td>
        <td>TOLOWERCASE(řetězec)</td>
        <td>
            řetězec malými písmeny
        </td>
    </tr>
    <tr>
        <td>TOUPPERCASE</td>
        <td>řetězec</td>
        <td>1</td>
        <td>TOUPPERCASE(řetězec)</td>
        <td>
            řetězec velkými písmeny
        </td>
    </tr>
    <tr>
        <td>FINDSUBSTRING</td>
        <td>číslo</td>
        <td>2</td>
        <td>FINDSUBSTRING(řetězec, řetězec)</td>
        <td>
            pozice prvního výskytu druhého parametru uvnitř prvního, null pokud neexistuje
        </td>
    </tr>

</table>

Dále je možné používat matematické funkce *ABS, SQRT, LOG, LOG10, SIN, COS,
    TAN, ASIN, ACOS, ATAN, SINH, COSH* a *TANH*.
    
Ke každému operátoru též existuje ekvivalentní funkce. Kromě již zmíněných to jsou *IF, EQ, NEQ, GT, GTE, LT, LTE, MINUS, DIV, MOD, POW, UPLUS, UMINUS* a *NEG*.

## Argumenty funkcí dané regulárním výrazem

Ve výrazech lze zapsat argumenty funkcí pomocí regulárních výrazů.
Vyhledávání probíhá na identifikátorech definovaných polí hodnocení a je <strong>case insensitive</strong>.
Regulární výrazy lze použít pouze jako parametry funkcí a jsou uvozeny zpětným apostrofem (znakem <strong>`</strong>, ascii 96).

Všechny nalezené sloupce jsou dosazené jako argumenty funkce v **abecedním pořadí**.
Pokud máme například definovány body za aktivitu pro každé cvičení pojmenované `ac1, ac2, ..., ac13`, lze potom všechny tyto hodnoty sečíst takto: ``SUM(`ac\d+`)``.
Podobně jdou regulární výrazy použít i u ostatních funkcí s variabilním počtem argumentů.

Ve zkratce:
<table>
    <tr>
        <td>x</td>
        <td>znak 'x'</td>
    </tr>
    <tr>
        <td>[abc]</td>
        <td>znak 'a', 'b' nebo 'c'</td>
    </tr>
    <tr>
        <td>\d</td>
        <td>číslo</td>
    </tr>
    <tr>
        <td>x+</td>
        <td>opakování jednou a více krát</td>
    </tr>
    <tr>
        <td>x*</td>
        <td>opakování nula a více krát</td>
    </tr>
    <tr>
        <td>xyz|abc</td>
        <td>xyz nebo abc</td>
    </tr>
</table>
Vyčerpávající dokumentace regulárních výrazů je dostupná v
<a href="https://docs.oracle.com/javase/7/docs/api/java/util/regex/Pattern.html">
    dokumentaci třídy java.util.regex.Pattern.
</a>

<table>
    <tr>
        <th>Výraz</th>
        <th>Přijaté argumenty</th>
        <th>Odmítnuté argumenty</th>
    </tr>
    <tr>
        <td>test\d+</td>
        <td>test1, test2, ..., test12</td>
        <td>test, testC</td>
    </tr>
    <tr>
        <td>f1*</td>
        <td>f, f1, f111111</td>
        <td>f2, 1f</td>
    </tr>
    <tr>
        <td>(hw|test)_\w*</td>
        <td>hw_a, hw_b, test_midterm</td>
        <td>hw, test_12</td>
    </tr>
</table>


## Pomocné proměnné ve výrazu

Ve výrazu lze definovat pomocné proměnné.
Všechny deklarace se musí nacházet na začátku výrazu a být oddělené středníkem.

Syntaxe je ve tvaru:
```
[datový typ] [identifikátor] = [výraz] , [identifikátor] = [výraz] , ... ; ...
```

Datový typ může být:
<table>
    <tr>
        <td>NUMBER</td>
        <td>číslo</td>
    </tr>
    <tr>
        <td>STRING</td>
        <td>řetězec</td>
    </tr>
    <tr>
        <td>BOOLEAN</td>
        <td>boolean</td>
    </tr>
    <tr>
        <td>AUTO</td>
        <td>datový typ určen automaticky z přiřazeného výrazu</td>
    </tr>
</table>

Příklad:
```
number minimum = 50, points = SUM(`test\d+`);
boolean conditionsMet = assessment && points >= minimum;
string passMark = MARK(points);
auto failMark = 'F';

conditionsMet ? passMark : failMark
```

## Interní proměnné

Ve vzorcích lze pomocí interních proměnných přistupovat k informacím o studentech nebo předmětu a jeho definicích.
Kromě informacích o definicích všechny proměnné **začínají symbolem $** a tím je editor začne napovídat.
K dispozici jsou následující proměnné:

#### Informace o studentech
<table>
    <tr>
        <th>Proměnná</th>
        <th>Datový typ</th>
        <th>Popis</th>
    </tr>
    <tr>
        <td>$USERNAME</td>
        <td>Text</td>
        <td>Uživatelské jméno</td>
    </tr>
    <tr>
        <td>$FIRSTNAME</td>
        <td>Text</td>
        <td>Jméno</td>
    </tr>
    <tr>
        <td>$LASTNAME</td>
        <td>Text</td>
        <td>Přijmení</td>
    </tr>
    <tr>
        <td>$FULLNAME</td>
        <td>Text</td>
        <td>Celé jméno</td>
    </tr>
    <tr>
        <td>$PERSONAL_NUMBER</td>
        <td>Text</td>
        <td>Osobní číslo (z KOSu)</td>
    </tr>
</table>

#### Informace o paralelkách studenta
Obsahují informace o různých paralelkách studenta podle jejich typů.
Možné typy jsou přednáška, cvičení a laboratoř a z nich jsou dostupné ty, které předmět používá.
**Číslo paralelky** je číslo viditelné v rozvrhu (např. 101).
**Kód paralelky** je identifikátor z KOSu unikátní napříč všemi předměty a semestry. 
<table>
    <tr>
        <th>Proměnná</th>
        <th>Datový typ</th>
        <th>Popis</th>
    </tr>
    <tr>
        <td>$LECTURE_PARALLEL_NUMBER</td>
        <td>Text</td>
        <td>Číslo přednáškové paralelky</td>
    </tr>
    <tr>
        <td>$TUTORIAL_PARALLEL_NUMBER</td>
        <td>Text</td>
        <td>Číslo paralelky cvičení</td>
    </tr>
    <tr>
        <td>$LABORATORY_PARALLEL_NUMBER</td>
        <td>Text</td>
        <td>Číslo paralelky laboratoře</td>
    </tr>
    <tr>
        <td>$LECTURE_PARALLEL_CODE</td>
        <td>Text</td>
        <td>Unikátní identifikátor přednáškové paralelky</td>
    </tr>
    <tr>
        <td>$TUTORIAL_PARALLEL_CODE</td>
        <td>Text</td>
        <td>Unikátní identifikátor paralelky cvičení</td>
    </tr>
    <tr>
        <td>$LABORATORY_PARALLEL_CODE</td>
        <td>Text</td>
        <td>Unikátní identifikátor paralelky laboratoře</td>
    </tr>
</table>

#### Informace o předmětu
<table>
    <tr>
        <th>Proměnná</th>
        <th>Datový typ</th>
        <th>Popis</th>
    </tr>
    <tr>
        <td>$COURSE_CODE</td>
        <td>Kód předmětu (např. BI-PA1)</td>
    </tr>
    <tr>
        <td>$SEMESTER_CODE</td>
        <td>Kód semestru (např. B191)</td>
    </tr>
</table>

#### Informace o vytvořených definicích hodnocení
Obsahují informace o vytvořených definicích.
Mají vždy tvar [identifikátor].[proměnná] (např. test1.MIN\_REQUIRED).
Pro přístup k informacím o aktuálně editované definici lze místo identifikátoru použít **$THIS** (např. $THIS.MIN\_REQUIRED)
<table>
    <tr>
        <th>Proměnná</th>
        <th>Datový typ</th>
        <th>Popis</th>
    </tr>
    <tr>
        <td>[identifikátor].MIN</td>
        <td>Číslo</td>
        <td>Minimum číselné definice</td>
    </tr>
    <tr>
        <td>[identifikátor].MIN_REQUIRED</td>
        <td>Číslo</td>
        <td>Požadované minimum číselné definice</td>
    </tr>
    <tr>
        <td>[identifikátor].MAX</td>
        <td>Číslo</td>
        <td>Maximum číselné definice</td>
    </tr>
    <tr>
        <td>[identifikátor].IDENTIFIER</td>
        <td>Text</td>
        <td>Identifikátor definice</td>
    </tr>
    <tr>
        <td>[identifikátor].HIDDEN</td>
        <td>Boolean</td>
        <td>Zda je definice skrytá</td>
    </tr>
</table>

