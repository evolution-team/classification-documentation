# Changelog aplikace Klasifikace

## Prosinec 2020

* Přidány časy začátku jednorázových akcí v seznamu skupin studentů.
* Opraveno nesprávné řazení notifikací podle data v aplikaci Android.
* Funkce `MARK` pro převod bodů na známku už umí zpracovat i `null`.

## Leden 2020

* Odkaz na předmět `https://grades.fit.cvut.cz/courses/{kód předmětu}` přesměruje přihlášeného uživatele na stránku podle své role.
    * Studenty přesměruje na studentovu klasifikaci.
    * Učitele přesměruje na stránku pro hodnocení studentů
    * Pro více informací vizte [tento issue](https://gitlab.fit.cvut.cz/evolution-team/classification-issues/issues/168).
* Opravena nesprávně zobrazující se chybová hláška "missing variable declaration" při filtrování studentů výrazem.
    * Pro více informací vizte [tento issue](https://gitlab.fit.cvut.cz/evolution-team/classification-issues/issues/176).
* Opraveno lagující scrollování kolečkem na myši, pokud měl uživatel při scrollování kurzor na nějakém inputu tabulky.

## Září 2019

* Zrušena možnost změny číselných hodnot kolečkem (a touchpadem) v hodnotících tabulkách.
    * Pro více informací vizte [tento issue](https://gitlab.fit.cvut.cz/evolution-team/classification-issues/issues/142).
* Přidán odkaz na Android aplikaci.
* Opravené nesprávné odkazy v přehledech na úvodní stránce. Nyní budou fungovat i ve chvíli, kdy je přepnuto do jiného semestru než současného.
* Přidán speciální odkaz, který otevře studentovo hodnocení v posledním semestru v současném studiu, kdy měl předmět zapsaný.
    * Odkaz je ve formátu `https://grades.fit.cvut.cz/courses/{kód předmětu}/my-classification/latest`.
    * Pro více informací vizte [tento issue](https://gitlab.fit.cvut.cz/evolution-team/classification-issues/issues/150).
* Studenti bez validního ČVUT usernamu (např. studenti z jiných škol) se nebudou zobrazovat v tabulkách hodnocení.
* Přidány prohlížečové push notifikace.
* Přidána možnost omezení rozsahu hodnot u řetězcových polí.
* Přidána možnost použít interní proměnné ve vzorcích.
    * Pro více informací vizte [dokumentaci vzorců](https://grades.fit.cvut.cz/documentation/expressions).
* Opraveno zaseknutí stránky při prvním přihlášení.

## Březen 2019

* V nastavení velikosti stránky v tabulkách hodnocení je možné klávesou enter potvrdit novou velikost. 
* Zobrazení správných čísel řádků v tabulkách hodnocení i na druhé, třetí, ... stránce.
* Řazení v tabulkách hodnocení řadí null hodnoty jako poslední při vzestupném i sestupném řazení.
* Opraveny nefunkční html title v menu u názvů předmětu.
* Tabulka v "Hodnocení předmětu" by měla správně změnit velikost po změně velikosti okna prohlížeče.
* Nyní lze zadávat nové řádky do poznámky k hodnocení v "Podrobném hodnocení".
* Opraveny nefunkční odkazy na dokumentaci.
* Opravena chyba funkce `ITH()`, která se nechovala správně, pokud jediný ne null argument byl poslední předaný.
* Přidán chybějící překlad pro seminární a konzultační paralelky.
* Opraveno nefunkční skrývání předmětů.

## Leden 2019

### Novinky

* Skupina studentů "Všechny moje paralelky" byla rozdělena na "Všechny moje přednáškové paralelky", totéž se cvičeními a laboratořemi.
* V menu "skupina studentů" se jako první budou zobrazovat skupiny, které přihlášený uživatel učí.
* V "Hodnocení studentů" je možné zobrazit si identifikátor paralelek.
* V "Hodnocení studentů" je možné zobrazit si paralelky a ročníky studentů.
* Nový vzhled v "Moje klasifikace" pro studenty.
* Při filtrování studentů v "Hodnocení studentů" podle jména nebo usernamu už není potřeba psát háčky a čárky a není nutné dodržovat pořadí jmen.
    * Pro více informací vizte [tento issue](https://gitlab.fit.cvut.cz/evolution-team/classification-issues/issues/34).
* Možnost skutečně zobrazit vše v tabulce v "Hodnocení studentů".
    * Pro více informací vizte [tento issue](https://gitlab.fit.cvut.cz/evolution-team/classification-issues/issues/88).
* V nastavení dovedností předmětu je možné vytvořit si vlastní dovednosti.
    * Toto zatím nefunguje při definování sloupce v "Definice hodnocení" -- je tedy nutné přepnout na nastavení dovedností.
* Přidána čísla řádků do tabulky v "Hodnocení studentů".
* Větší granularita chybových hlášek způsobené KOSem, už všechno není zametené pod "KOSapi nedostupné".
* Nová domovská stránka, na které budou přehledy z předmětů, které přihlášený uživatel studuje nebo učí.
    * U předmětů, které uživatel studuje, se zobrazuje přehled skládající se z získaných bodů, zápočtu a výsledné známky.
    * U předmětů, které uživatel vyučuje, se zobrazují statistiky předmětu, mezi nimiž patří například počet získaných zápočtu, průměrná známka.
    * Tyto informace se zobrazí, pokud je možné je vytáhnout z definic předmětu. Je potřeba tedy správně definovat hodnocení.
* Přístup aplikací přes API.
* Viditelnější odkaz na issue tracker.

### Opravy

* V textech notifikací se budou vypisovat pouze ta pole, která se změnila.
* Opraven bug v "Hodnocení studentů", kdy se při řazení sloupečku prolínala prázdná pole se seřazenými vyplněnými poli.
* Navýšena maximální velikost řetězce v buňkách tabulek.
* Opravena gramatická chyba (chybějící čárka) na titulní stránce v oslovení.
* Překlad skillů na dovednosti.
* Spousty drobných oprav stylů.
* Odstraněna drop down menu v místech, kde v importu nebo exportu existuje pouze jedna možnost zdroje či cíle.
* Opraveny některé chyby se zobrazením času (např. 16:0 místo 16:00).
* Opraven bug v "Hodnocení studentů", kdy kliknutí na tlačítko "Najít" v hledání výrazem nic nedělalo.
* Kliknutí na text notifikace označí notifikaci jako přečtenou. Dříve bylo potřeba kliknout na to kolečko.
* Opraven bug, kdy se 50 bodů zobrazilo jako 'F', 60 bodů jako 'E', atd.
* Opravena nesprávná aktualizace času při změně hodnocení.
* Opravena chyba, kdy studenti bez usernamu shodili aplikaci.

## Červen 2018

* Ve vzorcích lze vytvářet dočasné (lokální) proměnné (viz dokumentace tvorba vzorců).
* Zvýraznění důležitých sloupců v tabulce studentů.
* Upozornění na chybějící definici zápočtu nebo zkoušky, pokud má tato položka podle KOSu existovat.
* Automatické vyplňování položek sloupců v definici předmětu.
* Vylepšené řazení předmětů.
* Opravení špatných chybových hlášek a přidání nových.
* Vylepšený import CSV.
* Zvýraznění aktuální řádky v tabulce studentů.
* Vylepšené přepínání mezi semestry.
* Drobné úpravy UX.

## Duben 2018

* Názvy předmětů bez definic jsou v menu vyšedlé.
* Přidáno osobní číslo do tabulky hodnocení (defaultně je skryté).
* Definice se mohou vnořovat.
* Přidány dočasné proměnné ve vzorcích definic.
* Výběr vyhledávaných položek lze rovnou potvrdit enterem (není potřeba vybírat z dropdown nabídky).
* Zprovozněny URL odkazy do minulých semestrů.
* K jednotlivým klasifikacím byly přidány časy posledních úprav.

## Březen 2018

* Zaveden changelog.
* Přidána skupina umělých studentů pro testování hodnocení.
* Hromadné vytváření definic hodnocení.
* Push notifikace.
* Okamžité zobrazení nového ohodnocení studentům bez obnovení stránky.
* Možnost pro učitele vidět hodnocení z pohledu studenta (odkaz u jména studenta v tabulce hodnocení).
* Stránkování tabulky hodnocení.
* Možnost poslat upozornění aktuálně vybraným studentům v hodnocení předmětu.
* Filtrování sloupců v tabulce hodnocení pro semestr nebo zkouškové (rozpoznáváno podle zvoleného vztahu k předmětu).
* Přidána jména učitelů k paralelkám.
