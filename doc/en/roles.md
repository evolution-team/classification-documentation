# User roles and permissions

## User roles linked to a course

The following roles only apply for a specific course. For an example a teacher of BIE-AAG course isn't automatically a teacher of BIE-PA1 course.

List of roles:
* teacher,
* editor,
* guarantor.

List of actions:
* grade students,
* define course classification,
* add editors.

Teachers can only grade students. A teacher can grade all students of a course he/she teaches.

Editor can alter course classification. Editors of a course are implicitly teachers as well.

Guarantor has the same permissions as an editor. Additionally guarantors can add editors.

A summary can be found in a table below.

|           | Grade students | Define course classification | Add editors |
|:---------:|:--------------:|:----------------------------:|:-----------:|
| Teacher   |       [x]      |                              |             |
| Editor    |       [x]      |              [x]             |             |
| Guarantor |       [x]      |              [x]             |     [x]     |

Teacher and guarantor roles are granted using information from KOS. Editor permission has to be manually granted by the guarantor of a course. Editor role applies to all semesters so it needs to be granted just once.

## Roles independent from courses

The following roles are not linked to a course.

List of roles:
* head of deparment,
* department "scheduler.

Head of department has the same permissions as a guarantor of all courses under his/her department.

These roles are assigned using information from KOS. Department scheduler is, just like editor, internal role so it needs to be manually assigned by the head of department and also applies to all semesters.

There can be multiple editors and department schedulers.
