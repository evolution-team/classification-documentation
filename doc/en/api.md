# API

It is possible to access Grades resources over REST api. Api is **available to all users** (students also).

Api is also available to **external aplications** approved for a course by garant (or editor) from within Grades.
Usable for automatic evaluators like Progtest or Marast.

Valid **bearer token** is required (see <https://rozvoj.fit.cvut.cz/Main/oauth2>).
For user access use `user-read` and `user-write` scopes, for application access use `course-restricted` scope.
Detailed tutorial will be written soon. Feel free to ask development team for help (contact below).

**Api is mostly final and detailed documentation is gradually being written.**

Documentation with an interface to try calls is here: <https://grades.fit.cvut.cz/api/v1/swagger-ui.html>

**Feel free to ask development team for help.**
Contact plachste(at)fit.cvut.cz
