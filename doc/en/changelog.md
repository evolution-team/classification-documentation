# Changelog of Klasifikace

## December 2020

* Added start times to one-time events in student group lists.
* Fixed incorrect date sorting of notifications in Android app.
* Function `MARK` for converting points to a grade is now able to process `null`.

## January 2020

* Link `https://grades.fit.cvut.cz/courses/{course code}` redirects the logged in user to a page according to their role.
    * Students are redirected to their classification.
    * Teachers are redirected to course classification.
    * For more information see [this issue](https://gitlab.fit.cvut.cz/evolution-team/classification-issues/issues/168).
* Fixed incorrect error message "missing variable declaration" when filtering students by an expression.
    * For more information see [this issue](https://gitlab.fit.cvut.cz/evolution-team/classification-issues/issues/176).
* Fixed laggy mouse wheel scrolling, when the mouse cursor was placed on some classification table input.

## September 2019

* Numeric inputs can no longer be modified using mouse wheel (or touch pad).
    * For more information see [this issue](https://gitlab.fit.cvut.cz/evolution-team/classification-issues/issues/142).
* Added a link to Android application.
* Fixed incorrect links in overviews on title page. Now they will work when the selected semester is not the current one.
* Added a special link, which open student's classification in the most recent semester in current study, in which student had enrolled in the course.
    * The link is in the format `https://grades.fit.cvut.cz/courses/{course code}/my-classification/latest`.
    * For more information see [this issue](https://gitlab.fit.cvut.cz/evolution-team/classification-issues/issues/150).
* Students without a valid CTU username (e.g. students from other schools) will not be displayed in classification tables.
* Added browser push notifications.
* Added string enums.
* Expressions now support internal variables.
    * For more information see [documentation for expressions](https://grades.fit.cvut.cz/documentation/expressions).
* Fixed an infinite loading on first login.

## March 2019

* Classification table page size setting can be confirmed with enter key.
* Row numbers should display correct number on second, third, ... page in classification tables.
* Sorting in classification tables now displays null values as last when sorting in descending order as well as when sorting in ascending order.
* Fixed broken html titles in menu in course names.
* Classification tables now resize correctly, when the browser window size changes.
* Now it's possible to enter new lines in classification notes.
* Fixed broken documentation links.
* Fixed `ITH()` function's broken behaviour, when the only non null argument was the last one. 
* Fixed missing translation for seminar and consultation parallels.
* Fixed broken course hiding.

## January 2019

### New features

* Student group "All my parallels" has been split into "All my lecture parallels" and similarly with tutorials and laboratories.
* In "Student group" selector groups taught by currently logged in user are displayed first.
* Parallel id's can be displayed in "Course classification".
* Student's parallel and grade can be displayed in "Course classification".
* New design in "My classification" for students.
* Filtering students in "Course classification" now ignores accents and ordering of queried names.
    * For more information see [this issue](https://gitlab.fit.cvut.cz/evolution-team/classification-issues/issues/34) (in Czech only).
* Display all in "Course classification" now truly displays all.
    * For more information see [this issue](https://gitlab.fit.cvut.cz/evolution-team/classification-issues/issues/88) (in Czech only).
* Skills can be created in skills settings.
    * Doesn't work when defining course classification yet -- has to be done from "Skills settings".
* Added row numbers in "Course classification".
* Better granularity of error messages caused by KOS, i.e. there will be more than just "KOSapi unavailable".
* New home page with course overviews.
    * Course overviews of courses studied by user display his assessment state, etc.
    * Course overviews of courses taught by user display information such as the number of enrolled students, number of obtained assessments, etc.
    * Note these information display only if they can be extracted from classification definition of courses. Therefore it is necessary to correctly define classification.
* Applications can access Grades using API.
* More visible link to the issue tracker.

### Fixes

* Only changed fields will be displayed in the body of notifications.
* Fixed bug in "Course classification", where empty fields mixed with filled in fields when sorting.
* Increased maximum input length of inputs in course classification, detailed classification, etc.
* Fixed a grammar error on title page (a missing comma in the user greeting).
* Far too many styling fixes to list.
* Removed dropdown menus from places, where there was only a single option.
* Fixed time display issues.
* Fixed a bug where pressing "Find" button in "Course classification" in filtering did nothing.
* Clicking on a notification now marks it as read.
* Fixed a bug where 50 points displayed as 'F', 60 points as 'E', etc.
* Fixed incorrect time of last change in classifications.
* Fixed a bug where students without a username crashed the application.

## June 2018

* Temporary variables can be defined in expressions (see expression tutorial documentation page).
* Warning appears when the course is missing assessment or exam column when it is supposed to exist according to KOS.
* Highlighting of important columns in the table of students.
* Auto-filling of column's entries in course's definition.
* Improved course sorting.
* Fixed inappropriate error messages and added new ones.
* Improved CSV import.
* Highlighting of the current row in the table of students.
* Improved swapping between semesters.
* Minor UX tweaks.

# April 2018

* Courses without definitions have a grayed out title in the menu
* Added personal number to course evaluation table.
* Nested definitions.
* Variable declaration in course classification definitions.
* Item search query can be made without having to select the exact match from the dropdown menu.
* Fixed broken links to non-current semesters.
* 'Last updated' timestamp added to classifications.

# March 2018

* Created this changelog
* Added mock students to each course for testing.
* Multiple classification definitions can be created at once.
* Push notifications and immediate display of new grades without refreshing the page.
* Clicking on a student's username in a smart table will display student's view of the course classification.
* Classification table split into multiple pages.
* Sending notifications to currently displayed students in the classification table.
* Added a classification table filter by whether the classification occurred during the exam period or during the semester.
* Added teacher names to parallels.
